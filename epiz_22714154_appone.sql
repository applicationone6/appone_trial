-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: sql309.byetcluster.com
-- Generation Time: Jul 06, 2020 at 08:41 AM
-- Server version: 5.6.47-87.0
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `epiz_22714154_appone`
--

-- --------------------------------------------------------

--
-- Table structure for table `collage_info`
--

CREATE TABLE `collage_info` (
  `id` int(15) NOT NULL,
  `college_name` varchar(60) NOT NULL,
  `address` varchar(70) NOT NULL,
  `state` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coll_stream`
--

CREATE TABLE `coll_stream` (
  `id` int(15) NOT NULL,
  `coll_id` int(15) NOT NULL,
  `stream` varchar(50) NOT NULL,
  `algo` varchar(15) NOT NULL,
  `department` varchar(100) NOT NULL,
  `fees` int(10) NOT NULL,
  `min_aggregate` decimal(10,0) NOT NULL,
  `seat` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `counselling_process`
--

CREATE TABLE `counselling_process` (
  `id` int(15) NOT NULL,
  `collage_id` int(15) NOT NULL,
  `best_of_4` int(10) NOT NULL,
  `best4_plus_one` int(5) NOT NULL,
  `total_ag` int(10) NOT NULL,
  `stream1` varchar(10) NOT NULL,
  `stream2` varchar(10) NOT NULL,
  `stream3` varchar(20) NOT NULL,
  `stream4` varchar(20) NOT NULL,
  `stream5` varchar(20) NOT NULL,
  `stream6` varchar(20) NOT NULL,
  `stream7` varchar(20) NOT NULL,
  `stream8` varchar(20) NOT NULL,
  `stream1_marks` int(5) NOT NULL,
  `stream2_marks` int(5) NOT NULL,
  `stream3_marks` int(5) NOT NULL,
  `stream4_marks` int(5) NOT NULL,
  `stream5_marks` int(5) NOT NULL,
  `stream6_marks` int(5) NOT NULL,
  `stream7_marks` int(5) NOT NULL,
  `stream8_marks` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `educational_secondary`
--

CREATE TABLE `educational_secondary` (
  `id` int(15) NOT NULL,
  `student_id` int(15) NOT NULL,
  `sec_role_no` int(20) NOT NULL,
  `sec_board_name` varchar(50) NOT NULL,
  `sec_registration_no` int(50) NOT NULL,
  `sec_school_name` varchar(50) NOT NULL,
  `sec_school_address` varchar(70) NOT NULL,
  `sec_sub1` varchar(20) NOT NULL,
  `sec_sub2` varchar(20) NOT NULL,
  `sec_sub3` varchar(20) NOT NULL,
  `sec_sub4` varchar(20) NOT NULL,
  `sec_sub5` varchar(20) NOT NULL,
  `sec_sub6` varchar(20) NOT NULL,
  `sec_sub7` varchar(20) NOT NULL,
  `sec_marks_sub1` int(5) NOT NULL,
  `sec_marks_sub2` int(5) NOT NULL,
  `sec_marks_sub3` int(5) NOT NULL,
  `sec_marks_sub4` int(5) NOT NULL,
  `sec_marks_sub5` int(5) NOT NULL,
  `sec_marks_sub6` int(5) NOT NULL,
  `sec_marks_sub7` int(5) NOT NULL,
  `sec_full_marks_sub1` int(10) NOT NULL,
  `sec_full_marks_sub2` int(10) NOT NULL,
  `sec_full_marks_sub3` int(10) NOT NULL,
  `sec_full_marks_sub4` int(10) NOT NULL,
  `sec_full_marks_sub5` int(10) NOT NULL,
  `sec_full_marks_sub6` int(10) NOT NULL,
  `sec_full_marks_sub7` int(10) NOT NULL,
  `cgpa_sub1` int(5) NOT NULL,
  `cgpa_sub2` int(5) NOT NULL,
  `cgpa_sub3` int(5) NOT NULL,
  `cgpa_sub4` int(5) NOT NULL,
  `cgpa_sub5` int(5) NOT NULL,
  `cgpa_sub6` int(5) NOT NULL,
  `cgpa_sub7` int(5) NOT NULL,
  `sec_total_marks` int(5) NOT NULL,
  `sec_total_full_marks` int(5) NOT NULL,
  `sec_percentage` decimal(5,0) NOT NULL,
  `sec_total_avg_cgpa` int(5) NOT NULL,
  `sec_percentage_cgpa` decimal(5,0) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `education_higher_sec`
--

CREATE TABLE `education_higher_sec` (
  `id` int(15) NOT NULL,
  `student_id` int(15) NOT NULL,
  `hs_role_no` int(20) NOT NULL,
  `hs_board_name` varchar(50) NOT NULL,
  `hs_registration_no` int(50) NOT NULL,
  `hs_school_name` varchar(50) NOT NULL,
  `hs_school_address` varchar(70) NOT NULL,
  `hs_sub1` varchar(20) NOT NULL,
  `hs_sub2` varchar(20) NOT NULL,
  `hs_sub3` varchar(20) NOT NULL,
  `hs_sub4` varchar(20) NOT NULL,
  `hs_sub5` varchar(20) NOT NULL,
  `hs_sub6` varchar(20) NOT NULL,
  `hs_marks_sub1` int(5) NOT NULL,
  `hs_marks_sub2` int(5) NOT NULL,
  `hs_marks_sub3` int(5) NOT NULL,
  `hs_marks_sub4` int(5) NOT NULL,
  `hs_marks_sub5` int(5) NOT NULL,
  `hs_marks_sub6` int(5) NOT NULL,
  `hs_full_marks_sub1` int(15) NOT NULL,
  `hs_full_marks_sub2` int(15) NOT NULL,
  `hs_full_marks_sub3` int(15) NOT NULL,
  `hs_full_marks_sub4` int(15) NOT NULL,
  `hs_full_marks_sub5` int(15) NOT NULL,
  `hs_full_marks_sub6` int(15) NOT NULL,
  `hs_total_marks` int(5) NOT NULL,
  `hs_total_full_marks` int(5) NOT NULL,
  `hs_percentage` decimal(5,0) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personal_info`
--

CREATE TABLE `personal_info` (
  `id` int(15) NOT NULL,
  `std_fname` varchar(50) NOT NULL,
  `std_lname` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `category` varchar(10) NOT NULL,
  `aadharno` int(20) NOT NULL,
  `mother_fname` varchar(50) NOT NULL,
  `mother_lname` varchar(50) NOT NULL,
  `father_fname` varchar(50) NOT NULL,
  `father_lname` varchar(50) NOT NULL,
  `father_contact_no` int(12) NOT NULL,
  `father_email` varchar(50) NOT NULL,
  `address1` varchar(50) NOT NULL,
  `address2` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `district` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `ph_no` int(12) NOT NULL,
  `alter_no` int(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stream`
--

CREATE TABLE `stream` (
  `id` int(15) NOT NULL,
  `s_code` varchar(70) NOT NULL,
  `stream_name` varchar(100) NOT NULL,
  `department` varchar(70) NOT NULL,
  `sub1` varchar(50) NOT NULL,
  `sub2` varchar(50) NOT NULL,
  `sub3` varchar(50) NOT NULL,
  `sub4` varchar(50) NOT NULL,
  `sub5` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `upload_doc`
--

CREATE TABLE `upload_doc` (
  `id` int(15) NOT NULL,
  `customer_id` int(15) NOT NULL,
  `photo` varchar(20) DEFAULT NULL,
  `signature` varchar(20) DEFAULT NULL,
  `sec_admitcard` varchar(20) DEFAULT NULL,
  `sec_result` varchar(20) DEFAULT NULL,
  `hs_result` varchar(20) DEFAULT NULL,
  `address_proof` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collage_info`
--
ALTER TABLE `collage_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coll_stream`
--
ALTER TABLE `coll_stream`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `counselling_process`
--
ALTER TABLE `counselling_process`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id` (`collage_id`);

--
-- Indexes for table `educational_secondary`
--
ALTER TABLE `educational_secondary`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_id` (`student_id`);

--
-- Indexes for table `education_higher_sec`
--
ALTER TABLE `education_higher_sec`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_id` (`student_id`);

--
-- Indexes for table `personal_info`
--
ALTER TABLE `personal_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `stream`
--
ALTER TABLE `stream`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `s_code` (`s_code`);

--
-- Indexes for table `upload_doc`
--
ALTER TABLE `upload_doc`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collage_info`
--
ALTER TABLE `collage_info`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `coll_stream`
--
ALTER TABLE `coll_stream`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `counselling_process`
--
ALTER TABLE `counselling_process`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `educational_secondary`
--
ALTER TABLE `educational_secondary`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `education_higher_sec`
--
ALTER TABLE `education_higher_sec`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `personal_info`
--
ALTER TABLE `personal_info`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `stream`
--
ALTER TABLE `stream`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `upload_doc`
--
ALTER TABLE `upload_doc`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
